package main

import (
	"fmt"
	"log"
	"strings"
	"time"

	"github.com/google/uuid"
	"github.com/jmoiron/sqlx"

	"github.com/Masterminds/squirrel"
	"github.com/golang-migrate/migrate/v4"
	"github.com/golang-migrate/migrate/v4/database/postgres"
	_ "github.com/golang-migrate/migrate/v4/source/file"
	_ "github.com/jackc/pgx/v4/stdlib"
)

func main() {
	// for educational purposes I use hardcoded connection string
	// to run the app via 'go run .' uncomment the string below
	// db, err := sqlx.Connect("pgx", "postgres://postgres:postgres@localhost:5432/postgres?sslmode=disable")
	db, err := sqlx.Connect("pgx", "postgres://postgres:postgres@postgres-0:5432/postgres?sslmode=disable")
	if err != nil {
		log.Fatal("cannot open postgres:", err)
	}
	driver, err := postgres.WithInstance(db.DB, &postgres.Config{})
	if err != nil {
		log.Fatal("cannot create driver:", err)
	}
	m, err := migrate.NewWithDatabaseInstance("file://migrations", "postgres", driver)
	if err != nil {
		log.Fatal("cannot create db instance:", err)
	}
	err = m.Up()
	if err != nil && !strings.Contains(err.Error(), "no change") {
		log.Fatal("cannot migrate db:", err)
	}

	sql, args, err := squirrel.Insert("test").
		Columns("id", "value").
		Values(uuid.NewString(), time.Now().String()).
		PlaceholderFormat(squirrel.Dollar).ToSql()
	if err != nil {
		log.Fatal("cannot create query:", err)
	}

	_, err = db.Exec(sql, args...)
	if err != nil {
		log.Fatal("cannot insert:", err)
	}

	res := make([]struct {
		ID    string `db:"id"`
		Value string `db:"value"`
	}, 0)

	err = db.Select(&res, "select id, value from test")
	if err != nil {
		log.Fatal("cannot select:", err)
	}

	fmt.Println(res)
}
