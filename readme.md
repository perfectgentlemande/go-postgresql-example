Pulling PostgreSQL:  
`docker pull postgres:14.1`  

Creating network:  
`docker network create db_network`  

Creating volume:  
`docker volume create postgres-vol`  

Running PostgreSQL:  
`docker run -it --rm -p 5432:5432 --name postgres-0 --network db_network --mount source=postgres-vol,target=/var/lib/postgresql/data -e POSTGRES_PASSWORD=postgres postgres:14.1`  

Running PostgreSQL (no volume):  
`docker run -it --rm -p 5432:5432 --name postgres-0 --network db_network -e POSTGRES_PASSWORD=postgres postgres:14.1`  

Building app image:
`docker build -t go-postgresql-app:v0.1.0 .`

Running app:
`docker run --name go-postgresql-app --network db_network -it go-postgresql-app:v0.1.0`