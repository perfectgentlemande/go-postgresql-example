FROM golang:1.17.3-alpine3.14 AS builder

ADD . /app
WORKDIR /app
RUN go build -o go-postgresql-app .

FROM alpine:3.14 AS app
WORKDIR /app
COPY --from=builder /app/migrations /app/migrations
COPY --from=builder /app/go-postgresql-app /app
CMD ["./go-postgresql-app"]